**This is the home page of [this documentation](http://wonderland.eode.studio/docs/prebuild-_introduction.html)**

## Introduction

### Version
> Released, stable

### Dependencies
- `Common`

## Motivation
Prebuild is inspired by unity network commands. With this module you can create custom commands for your game.

Arguments can be placed before classes fields and methods and execute a code generator script.
The script is written at the end of the script file in a custom region, it is just an helper, not a magic argument like unity arguments (unity generate code at compile time in dll. You can't see, modify or debug the generated code).

Prebuild code generation should be called in the main menu of wonderland or added in your own building procedure.

Remember to use that if you can't use inheritance or other. Use Prebuild with caution.

![Example](http://wonderland.eode.studio/Prebuild/prebuild1.png)

## Small generated code example
You can not change the value before a SayHello() call...
```C#
[UselessPrebuild]
public class MyBehaviour: Monobehaviour {
    [CustomVar]
    int _myValue;

    [CustomMethod]
    void hello() {
        print("hello");
    }

#region Prebuild
public int MyValue {
    set {
        if (_locked) {
            print("MyValue: The behaviour is locked !");
            return;
        }

        print("MyValue: " + value);
        _myValue = value;
    }
    get {
        return _myValue;
    }
}
bool _locked = true;
public void SayHello() {
    if (!_locked) return;
    _locked = false;
    hello();
}
#endregion
}
```

## Prebuild config
You can configure the prebuild path in the wonderland configuration windows.
To run prebuild use the menu `Wonderland/Prebuild/Run`.

> The prebuild operation rewrites files, it is recommanded to commit your project before running.

_(it is just a causion box, i never lost my work. It is relatively secure because all content is generated before the writing procedure)_

## External Cache file
Prebuild region can be generated in another file, see [Cache file generation](http://wonderland.eode.studio/docs/prebuild-GenerateExternal.html).

